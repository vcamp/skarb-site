## Hej hej! 👋

Chances are you are here because you searched for a **bilingual Polish-English dictionary for Kindle**, and found out that:

* Polish is not a supported language on Kindle (= no official dictionary)
* Third-party dictionaries lack the capability of linking inflected forms to headwords (for example: when searching for "psa" you're not redirected to "pies")

I found myself in the same position and felt very frustrated - as a language learner, my favourite way of improving is to read books and stories, and using the available dictionaries felt really clunky.

So... I decided to make my own dictionary, and to share it with anyone else that might find it useful. I decided to call the project **Skarb** - the Polish word for "treasure" or "hoard".

You can **download for free the latest Skarb release {{< download_link >}}**.

There are also some more details on how and why I made the dictionary scattered across the rest of this site.

Miłej lektury! 🇵🇱📚
