+++
title = "How to Install"
date = "2021-03-28T22:48:32+02:00"

#
# Set menu to "main" to add this page to
# the main menu on top of the page
#
menu = "main"

#
# description is optional
#
# description = "An optional description for SEO. If not provided, an automatically created summary will be used."

#
# tags are optional
#
# tags = []
+++

## Tested and supported models

I have tested and confirmed that Skarb works as intended on the following Kindle models:

* **4th generation Kindle**
* **5th generation Kindle & Kindle Paperwhite**

Unfortunately Amazon does not supply virtual emulators for Kindle devices, so I can't confirm the dictionary works with any model other than the ones listed above.

## How to install a Kindle dictionary

(note: I adapted most of this from the excellent guide at [Intangible Press](http://www.intangiblepress.com/help/kindle_dictionary_FAQ.html))

### 4th generation or later Kindle, Kindle Oasis, Kindle Paperwhite, or Kindle Voyage:

Download the latest dictionary release on your computer and extract the .MOBI file from the archive.

Using a USB cable, connect your Kindle to your computer. Transfer the .MOBI file to the "Dictionaries" folder in your Kindle.

Press the Home key. At the Home screen, select Menu, then Settings. At the Settings screen, the next step will depend on your device:

* On the Kindle, scroll down until you see "Dictionaries", then select it
* On the Kindle Oasis, Voyage, or Paperwhite, select "Language and Dictionaries", then "Dictionaries"

In the Dictionaries list, you can select a default dictionary for each language for which you have multiple dictionaries. For example, if you have dictionaries for English, Spanish and German, you will see these languages listed along with the current default dictionary for each. (If you only have one dictionary for a particular language, it is pre-selected as default and there is nothing you need to do.)

When multiple dictionaries are available for a particular language, either a right arrow or the text “change default” will be displayed in the right margin. To change the current selection on Kindle Oasis, Kindle Voyage, Kindle Paperwhite, or Kindle Touch, tap on it; to change it on a non-touch screen Kindle, select it with the navigation key, then press OK. A list of available dictionaries for that language will be displayed. Select the dictionary you want to use as default, then select OK.

### 2nd & 3rd generation Kindles

Download the latest dictionary release on your computer and extract the .MOBI file from the archive.

Using a USB cable, connect your Kindle to your computer. Transfer the .MOBI file to the "Dictionaries" folder in your Kindle.

Press the Home key. At the Home screen, select Menu, then Settings. At the Settings screen, press Menu again, then select Change Primary Dictionary. In the Dictionaries list, you can select a default dictionary for lookup. Highlight the dictionary you want to use, and then press OK.

### Kindle Fire (Fire OS 4+)

Unfortunately this type of device does not allow setting default dictionaries covering unsupported languages. There are guides elsewhere on the Internet on how to override the default English dictionary but the methods suggested do not seem very safe, so I wouldn't advise following them.

### Kindle for iOS, Kindle for Android

This section is pending direct testing on both applications.

### A note on eBook compatibility

The dictionary you select will only look up words for eBooks tagged in the same language. For example, if you highlight a word in an eBook tagged as German, you will only be able to use a dictionary that is tagged as German. See the description of your specific eBook(s) to verify this information.
