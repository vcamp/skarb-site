+++
title = "Contact"
date = "2021-06-28T22:48:41+02:00"

#
# Set menu to "main" to add this page to
# the main menu on top of the page
#
menu = "main"

#
# description is optional
#
# description = "An optional description for SEO. If not provided, an automatically created summary will be used."

#
# tags are optional
#
# tags = []
+++

## Project contacts

Send any appreciation, feature ideas, complaints or cease-and-desist notices to [skarbdictionary@gmail.com](mailto:skarbdictionary@gmail.com).
