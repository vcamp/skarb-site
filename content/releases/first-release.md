+++
title = "First Release"
date = "2021-03-28T22:51:39+02:00"

#
# description is optional
#
# description = "An optional description for SEO. If not provided, an automatically created summary will be used."

tags = []
+++

## Summary

First public release. Lemmas extracted from [Wiktionary](https://www.wiktionary.org/) (via [wikiextract](https://github.com/tatuylonen/wiktextract)) and inflected/conjugated forms generated through [morfeusz2](http://morfeusz.sgjp.pl/).

This version attempts to exclude inflected/conjugated forms used as headwords in Wiktionary.

## Release artifacts and statistics

Click [here](https://gitlab.com/vcamp/polish-english-dictionary/-/jobs/1134418832/artifacts/download?file_type=archive) to access the dictionary .MOBI file, a file with statistics on the dictionary and the number of excluded Wiktionary entries.


## Dictionary features

* 28302 lemmas
* Searching for an inflected form returns base lemma (psa --> pies)
