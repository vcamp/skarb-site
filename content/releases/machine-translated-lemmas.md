+++
title = "Machine Translated Lemmas"
date = "2021-06-30T21:50:20+02:00"

#
# description is optional
#
# description = "An optional description for SEO. If not provided, an automatically created summary will be used."

tags = []
+++

## Summary

Second public release. Core lemmas extracted from [Wiktionary](https://www.wiktionary.org/) (via [wikiextract](https://github.com/tatuylonen/wiktextract)) and inflected/conjugated forms generated through [morfeusz2](http://morfeusz.sgjp.pl/). Additional lemmas scraped from [SGJP](sgjp.pl) and translated using the Google Cloud Translate API.

This version attempts to exclude inflected/conjugated forms used as headwords in Wiktionary.

## Release artifacts and statistics

Click [here](https://gitlab.com/vcamp/skarb-code/-/jobs/1390315085/artifacts/download) to access the dictionary .MOBI file, a file with statistics on the dictionary and the number of excluded Wiktionary entries.


## Dictionary features

* 284553 lemmas (10x compared to previous release)
* Searching for an inflected form returns base lemma (psa --> pies)
