+++
title = "Project Resources"
date = "2021-03-28T22:48:41+02:00"

#
# Set menu to "main" to add this page to
# the main menu on top of the page
#
menu = "main"

#
# description is optional
#
# description = "An optional description for SEO. If not provided, an automatically created summary will be used."

#
# tags are optional
#
# tags = []
+++

## Project repository

If you want to have a deeper look into how the dictionary is created, see what additional features are in the pipeline or even contribute with a pull request or bugfix, you can check out the Gitlab repo [here](https://gitlab.com/vcamp/skarb-code).

## Corpus sources

* English-language [Wiktionary](https://en.wiktionary.org/wiki/Wiktionary:Main_Page)
* [SGJP](http://sgjp.pl/help/)

